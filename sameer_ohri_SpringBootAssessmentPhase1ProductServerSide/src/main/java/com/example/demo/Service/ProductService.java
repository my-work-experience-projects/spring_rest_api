package com.example.demo.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Model.Product;
import com.example.demo.Repository.ProductRepository;

@Service
public class ProductService {
	
	ProductRepository productRepo ;

	@Autowired
	public ProductService(ProductRepository productRepo) {
		super();
		this.productRepo = productRepo;
	}
	
	public Product createProduct (Product product) throws ProductCannotBeCreatedException {
		    if(product.getProductName().matches("[0-9]")) {
				throw new ProductCannotBeCreatedException("Product cannot be created...");

		    }
			productRepo.save(product);
		
		
		System.out.println("Product saved...");
		return product;
	}
	
	
	
	public List<Product> retrieveProducts () {
		
		return productRepo.findAll();
	}
	
	public Product retrieveProductById(long productId) throws ProductwithIDNotFoundException
	{
		Optional<Product> productOpt = productRepo.findProductByproductId(productId);
		if(!productOpt.isPresent())
		{
			throw new ProductwithIDNotFoundException("Product with the id "+productId+" not found");
		}
		return productOpt.get();
	}
	
	public Product retrieveProductByName(String productName) throws  ProductwithNameNotFoundException {
		Optional<Product> productOpt = productRepo.findByProductName(productName);
		if(!productOpt.isPresent())
		{
			throw new ProductwithNameNotFoundException("Product with the name "+productName+" not found");
		}
		return productOpt.get();
	}
	
	public Product updateProduct(Product product)
	{
		return productRepo.save(product);
	}
	
	public void deleteProduct(long productId)
	{
		//productRepo.deleteProductByproductId(productId);
		productRepo.deleteById(productId);
	}

}
