package com.example.demo.Service;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ProductCannotBeCreatedException extends Exception {

	public ProductCannotBeCreatedException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProductCannotBeCreatedException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public ProductCannotBeCreatedException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ProductCannotBeCreatedException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ProductCannotBeCreatedException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
