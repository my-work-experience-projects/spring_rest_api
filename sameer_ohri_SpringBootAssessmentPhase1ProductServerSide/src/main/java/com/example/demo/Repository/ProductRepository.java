package com.example.demo.Repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.Model.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {
	
	public Optional<Product> findProductByproductId(long id);
	public Optional<Product> findByProductName(String name);

}
