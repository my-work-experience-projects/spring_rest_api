package com.example.demo.Controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.demo.Model.Product;
import com.example.demo.Service.ProductCannotBeCreatedException;
import com.example.demo.Service.ProductService;
import com.example.demo.Service.ProductwithIDNotFoundException;
import com.example.demo.Service.ProductwithNameNotFoundException;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@CrossOrigin
@RestController
@RequestMapping("api/v1/products")
public class ProductController {

	ProductService productService;

	@Autowired
	public ProductController(ProductService productService) {
		super();
		this.productService = productService;
	}

	@Operation(summary = "Creating Products : ")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE),
			@Content(mediaType = MediaType.APPLICATION_XML_VALUE) }, headers = {
					@Header(name = "location", description = "uri associated with the created product") }, description = "Inserts the data/Creates the product"),
			@ApiResponse(responseCode = "404", description = "Product cannot be created") })

	@PostMapping
	public ResponseEntity<Product> createProduct(@Valid @RequestBody Product product)
			throws ProductCannotBeCreatedException {
		Product newProduct = productService.createProduct(product);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{productId}")
				.buildAndExpand(newProduct.getProductId()).toUri();
		return ResponseEntity.created(location).body(newProduct);
	}

	@Operation(summary = "Retrieving all Products : ")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", content = { @Content(mediaType = MediaType.APPLICATION_JSON_VALUE),
					@Content(mediaType = MediaType.APPLICATION_XML_VALUE) }, headers = {
							@Header(name = "location") }, description = "Retrieves all Products"),
			@ApiResponse(responseCode = "404", description = "Products not found") })

	@GetMapping
	public ResponseEntity<List<Product>> getProducts() {
		List<Product> products = productService.retrieveProducts();
		return ResponseEntity.ok(products);
	}

	@Operation(summary = "Retrieve products by ID : ")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE),
			@Content(mediaType = MediaType.APPLICATION_XML_VALUE) }, headers = {
					@Header(name = "location", description = "url to product") }, description = "Get the Product by ID"),
			@ApiResponse(responseCode = "404", description = "Product not found") })

	@GetMapping("{productId}")
	public ResponseEntity<Product> getProduct(@PathVariable("productId") long productId)
			throws ProductwithIDNotFoundException {
		Product product = productService.retrieveProductById(productId);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().build().toUri();
		return ResponseEntity.created(location).body(product);
	}

	@Operation(summary = "Retrieve products by Name : ")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE),
			@Content(mediaType = MediaType.APPLICATION_XML_VALUE) }, headers = {
					@Header(name = "location", description = "url to product") }, description = "Get the Product by Name"),
			@ApiResponse(responseCode = "404", description = "Product not found") })

	@GetMapping("/productName/{productName}")
	public ResponseEntity<Product> getProductByName(@PathVariable("productName") String productName)
			throws ProductwithNameNotFoundException {
		Product product = productService.retrieveProductByName(productName);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().build().toUri();
		return ResponseEntity.created(location).body(product);
	}

	@Operation(summary = "Updating the Products : ")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE),
			@Content(mediaType = MediaType.APPLICATION_XML_VALUE) }, headers = {
					@Header(name = "location", description = "url to product") }, description = "Takes the product to be updated : "),
			@ApiResponse(responseCode = "404", description = "Product not found") })

	@PutMapping("/{productId}")
	public ResponseEntity<Product> updateProduct(@PathVariable("productId") long productId,
			@RequestBody Product product) {
		Product updatedProduct = productService.updateProduct(product);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().build().toUri();

		return ResponseEntity.created(location).body(product);
	}

	@Operation(summary = "Deleting the Products : ")
	@ApiResponses(value = { @ApiResponse(responseCode = "200", content = {
			@Content(mediaType = MediaType.APPLICATION_JSON_VALUE),
			@Content(mediaType = MediaType.APPLICATION_XML_VALUE) }, headers = {
					@Header(name = "location", description = "url to product") }, description = "Takes the Product to be deleted"),
			@ApiResponse(responseCode = "404", description = "Product not found") })

	@DeleteMapping("/{productId}")
	public void removeProduct(@PathVariable("productId") long productId) {
		productService.deleteProduct(productId);
	}

}
