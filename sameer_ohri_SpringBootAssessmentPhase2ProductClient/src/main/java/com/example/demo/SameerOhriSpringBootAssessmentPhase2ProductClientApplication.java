package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;


@SpringBootApplication
public class SameerOhriSpringBootAssessmentPhase2ProductClientApplication {
	private final String PRODUCT_REST_BASE_URL = "http://localhost:8080/ShopperUserInterface/api/v1/products";

	public static void main(String[] args) {
		SpringApplication.run(SameerOhriSpringBootAssessmentPhase2ProductClientApplication.class, args);
	}
	

	@Bean
	public WebClient.Builder webClientBuilder()
	{
		return WebClient.builder();
	}
	
	@Bean
	public WebClient contactRestWebClient(WebClient.Builder builder)
	{
		return builder.baseUrl(PRODUCT_REST_BASE_URL)
				.defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
				.build();
	}
}
