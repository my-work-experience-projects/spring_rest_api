package com.example.demo.Service;

import java.util.List;

import com.example.demo.Model.Product;


public interface ProductRestClient {

	List<Product> getProducts();
	Product getProduct(long id);
	void UpdateProduct(Product product);
	Product saveProduct(Product product);
	void deleteProduct(long id);

}
