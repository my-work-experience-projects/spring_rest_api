package com.example.demo.Service;

import java.util.List;
import java.util.function.Supplier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import com.example.demo.Model.Product;

import reactor.core.publisher.Mono;
@Component
public class ProductWebClient implements ProductRestClient {
	
	WebClient prodClient;
	
	@Autowired
	    public ProductWebClient(WebClient prodClient) {
		super();
		this.prodClient = prodClient;
	}

	@Override
	public List<Product> getProducts() {
		return prodClient.get().retrieve().bodyToFlux(Product.class).collectList().block();

	}

	@Override
	public Product getProduct(long id) {
		return prodClient.get()
				.uri("/"+id)
				.retrieve()
				.onStatus(status -> status.value()==HttpStatus.NOT_FOUND.value(), 
				response -> Mono.error(new ProductwithIDNotFoundException("Product "+id+" not found")))
				.bodyToMono(Product.class)
				.block();
	}

	@Override
	public void UpdateProduct(Product product) {
		prodClient.put()
		.uri("/"+product.getProductId())
		.bodyValue(product).retrieve()
		.toBodilessEntity()
		.block();
		
	}

	@Override
	public Product saveProduct(Product product) {
		return prodClient.post()
				.bodyValue(product).retrieve().onStatus(status -> status.value()==HttpStatus.BAD_REQUEST.value(), 
						response -> Mono.error(new ProductCannotBeCreatedException("Product cannot be created..."))).bodyToMono(Product.class).block();
	}

	@Override
	public void deleteProduct(long id) {
		prodClient.delete()
		.uri("/"+id)
		.retrieve()
		.toBodilessEntity()
		.block();		
	}

}
