package com.example.demo.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Model.Product;


@Service
public class ProductService {
	
	ProductRestClient prodRestClient;

	@Autowired
	public ProductService(ProductRestClient prodRestClient) {
		super();
		this.prodRestClient = prodRestClient;
	}
	
	public List<Product> retrieveProducts() {

		return prodRestClient.getProducts();
	}
	
	public Product retrieveProduct(long id)
	{
		return prodRestClient.getProduct(id);
	}
	
	public Product createProduct(Product product)
	{
		return  prodRestClient.saveProduct(product);
	}
	
	public void putProduct(Product product) {
		prodRestClient.UpdateProduct(product);
	}
	
	public void deleteProduct(long id) {
		prodRestClient.deleteProduct(id);
	}
}


