package com.example.demo.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.Model.Product;
import com.example.demo.Service.ProductCannotBeCreatedException;
import com.example.demo.Service.ProductService;
import com.example.demo.Service.ProductwithIDNotFoundException;

@Controller
public class ProductController {
	
	ProductService productService ;

	@Autowired
	public ProductController(ProductService productService) {
		super();
		this.productService = productService;
	}
	
	@GetMapping("/createProduct")
	public String addProduct (Model model) {
		model.addAttribute("product", new Product());
		return "createProduct";
	}
	
	@PostMapping("/processProduct")
	public String processProduct (Product product) {
		//return "confirmation";
		Product newProduct = productService.createProduct(product);
		return "redirect:/"+newProduct.getProductId();

	}
	
	@GetMapping("/listProducts")
	public String getAllProducts(Model model)
	{
		List<Product> products = productService.retrieveProducts();
		model.addAttribute("products", products);
		return "list-products";
	}
	
	@GetMapping("/{productId}")
	public String getContact(@PathVariable long productId,Model model)
	{
		Product product = productService.retrieveProduct(productId);
		model.addAttribute("product", product);
		return "productDetails";
	}
	
	@GetMapping("/delete/{productId}")
	public String deleteProduct( @PathVariable("productId") long productId)
	{
		
		productService.deleteProduct(productId);
		return "redirect:/";
	}
	
	@GetMapping("/update/{productId}")
	public String updateProduct( @PathVariable("productId") long productId,Model model)
	{
		
		Product product = productService.retrieveProduct(productId);
		model.addAttribute("product", product);
		return "update-product";
	}
	
	@PostMapping("/updateProduct")
	public String updateContact(Product product)
	{
		
		 productService.putProduct(product);
		
		return "redirect:/";
	}
	@ExceptionHandler(ProductwithIDNotFoundException.class)
	public String handleProductNotFound(ProductwithIDNotFoundException prodNotFound, Model ra)
	{
		
		ra.addAttribute("error",prodNotFound.getMessage());
		return "productNotFound";
		
		
	}
	@ExceptionHandler(ProductCannotBeCreatedException.class)
	public String handleProductNotCreated(ProductCannotBeCreatedException prodCreate, Model ra)
	{
		
		ra.addAttribute("error",prodCreate.getMessage());
		return "productNotCreated";
		
		
	}
	

}
